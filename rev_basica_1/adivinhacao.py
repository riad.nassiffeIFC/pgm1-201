"""
Esse modulo irá conter o jogo de adivinhação.
"""
import random as ran
from jogo import Jogo

class JogoAdivinhacao(Jogo):

    def __init__(self, jogador_1, jogador_2, chances):
        super().__init__( "jogo de adivinhação", jogador_1, jogador_2)
        self.numero_secreto = ran.randint(0, 100)
        self.chances = chances
        self.tentativa = 0

    def verificar_numero(self, valor):
        if self.numero_secreto == valor:
            print("Parabéns, você acertou o número secreto!")
            return True
        elif self.numero_secreto > valor:
            print("Você precisa chutar um valor mais alto")
            return False 
        else:
            print("Você precisa chutar um valor mais baixo") 
        return False

    def jogar(self):
        sair = False
        print(self)
        self.tentativa = 0
        jogador = self.jogador_1
        while not sair:
            valor = int(input(f"{jogador} digite um número inteiro: "))
            #Verifica se acertou o número secreto.
            if self.verificar_numero(valor):
                break
            self.tentativa = self.tentativa + 1
            sair = self.chances <= self.tentativa
            print(self)
            if jogador == self.jogador_1:
                jogador = self.jogador_2
            else:
                jogador = self.jogador_1

        if sair:
            print(f"Melhor sorte na próxima partida, o número secreto era:{self.numero_secreto}")
        else:
            print(f"O jogador {jogador} ganhou a partida!!!")

    def __repr__(self) -> str:
        return f"Partida de {self.jogador_1} vs {self.jogador_2}! tentativa número {self.tentativa}"

if __name__ == "__main__":
    partida = JogoAdivinhacao("João", "Pedro", 4)
    partida.jogar()
    print(partida)