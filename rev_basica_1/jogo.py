class Jogo:

    def __init__(self, nome, jogador_1, jogador_2) -> None:
        self._nome = nome
        self.jogador_1 = jogador_1
        self.jogador_2 = jogador_2

    def imprimir_jogadores(self):
        print(f"Jogador 1: {self.jogador_1}")
        print(f"Jogador 2: {self.jogador_2}")

    def jogar():
        print("Esse método precisa ser sobrescrito!")


class JogoTabulero (object):
    
    def imprimir_jogadores(self):
        print("Método do objeto tipo JogoTabulero")
        print(f"Jogador 1: {self.jogador_1}")
        print(f"Jogador 2: {self.jogador_2}")