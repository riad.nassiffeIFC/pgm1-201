from jogo import Jogo, JogoTabulero


class ErroPosicao(Exception):

    def __init__(self):
        self.message = "Número inválido"

class JogoVelha(JogoTabulero, Jogo):

    def __init__(self, jogador_1, jogador_2) -> None:
        super().__init__("Jogo da Velha", jogador_1, jogador_2)
        self.tabulero = [["1","4","7"],
                         ["2","5","8"],
                         ["3","6","9"]
                        ]

    def imprimir_tabulero(self):
        indice_linha = 0
        for linha in self.tabulero:
            indice_linha += 1
            indice_coluna = 0
            for coluna in linha:
                indice_coluna += 1
                if coluna == "X":
                    print("X",end=" ")
                elif coluna == "O":
                    print("O",end=" ")
                else:
                    print(" ",end=" ")

                if indice_coluna != 3:
                    print(":", end=" ")
            print("")#print para realizar a quebra de linha
            if indice_linha != 3:
                print("----------")

    def executar_jogada(self):
        posicao = input("Digite a posição que você deseja jogar (exemplo 0,0):\n ")
        x, y = posicao.split(",")
        x = int(x)
        y = int(y)
        if x > 2 or x < 0:
            raise ErroPosicao()
        if y > 2 or y < 0:
            raise ErroPosicao()
        return (x,y)

    def verificar_fim(self):
        #Esse for verifica se alguma linha foi preenchida com somente um simbolo
        for linha in self.tabulero:
            if linha[0] == linha[1] and linha[1] == linha[2]:
                return True
        #Verificação por coluna
        for i in range(3):
            if self.tabulero[0][i]==self.tabulero[1][i] and self.tabulero[1][i]==self.tabulero[2][i]:
                return True
        #verificação por diagonal
        if self.tabulero[0][0]==self.tabulero[1][1] and self.tabulero[0][0]==self.tabulero[2][2]:
            return True
        if self.tabulero[0][2]==self.tabulero[1][1] and self.tabulero[1][1]==self.tabulero[2][0]:
            return True
        return False

    def validar_jogada(self, posicao):
        if self.tabulero[posicao[0]][posicao[1]] == "X" or \
           self.tabulero[posicao[0]][posicao[1]] == "O":
           return False
        return True
        

    def jogar(self):
        fim = False
        jogador = "O"
        trocar = True
        while not fim:
            if jogador == "X" and trocar:
                jogador = "O"
            elif jogador == "O" and trocar:
                jogador = "X"
            print(f"Vez do jogador {jogador}")
            self.imprimir_tabulero()
            try:
                posicao = self.executar_jogada()
                if self.validar_jogada(posicao):
                    self.tabulero[posicao[0]][posicao[1]] = jogador
                    fim = self.verificar_fim()
                    trocar = True
                else:
                    trocar = False
                    print("Jogada inválida, tente de novo!")
            except ErroPosicao:
                print("Posição inválida")
                trocar = False
        
        self.imprimir_tabulero()
        if jogador == "O":
            print(f"O jogador {self.jogador_2} ganhou!!!")
        else:
            print(f"O jogador {self.jogador_1} ganhou!!!")
    


if __name__ == "__main__":
    partida = JogoVelha("Pedro", "João")
    partida.imprimir_jogadores()
    partida.jogar()