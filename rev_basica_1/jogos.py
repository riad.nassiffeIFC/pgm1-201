import adivinhacao
import jogo_velha

jogo_escolhido = input("Digite o nome do jogo que vc quer jogar!")
jogador_1 = input("Digite o nome do jogador 1: ")
jogador_2 = input("Digite o nome do jogador 2: ")

if jogo_escolhido.lower() == "adivinhação":
    partida = adivinhacao.JogoAdivinhacao(jogador_1, jogador_2, 4)
else:# jogo_escolhido.lower() == "jogo da velha":
    partida = jogo_velha.JogoVelha(jogador_1, jogador_2)

partida.jogar()
partida.imprimir_jogadores()


