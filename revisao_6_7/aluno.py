class Aluno:

	def __init__(self, nome: str, matricula: str, prova_1: float,
                prova_2: float, trabalho: float) -> None:
		self.nome = nome
		self.matricula = matricula
		self.prova_1 = prova_1
		self.prova_2 = prova_2
		self.trabalho = trabalho
		
	def calcular_media(self) -> float:
		return round((self.prova_1*2.5 + self.prova_2*2.5 + self.trabalho*2)/7, 2)
		
	def calcular_pontos_final(self) -> float:
		if self.calcular_media() >= 6:
			return 0.0
		return (6 - self.calcular_media())*3