from aluno import Aluno

def test_criar_objeto():
    aluno = Aluno("Ríad", "2016546", 1.0, 7.0, 9.0)
    assert aluno.nome == "Ríad"
    assert aluno.matricula == "2016546"
    assert aluno.prova_1 == 1.0
    assert aluno.prova_2 == 7.0
    assert aluno.trabalho == 9.0

def test_calcular_media_abaixo_de_6():
    aluno = Aluno("Ríad", "2016546", 1.0, 7.0, 9.0)
    assert aluno.calcular_media() == 5.43

def test_calcular_media_acima_de_6():
    aluno = Aluno("Ríad", "2016546", 9.0, 7.0, 9.0)
    assert aluno.calcular_media() == 8.29

def test_calcular_media_igual_6():
    aluno = Aluno("Ríad", "2016546", 5.999, 6.0, 6.0)
    assert aluno.calcular_media() == 6.00