from livro import LivroFisico
from livro import LivroEletronico

class Biblioteca(object):

    def __init__(self):
        self.__livros = []

    def buscar_titulo(self, titulo: str) -> Livro:
        for livro in self.__livros:
            if livro.titulo == titulo:
                return livro
        return None

    def adicionar(self, fisico: bool, autor: str, assunto: str,
                  resumo: str, isbn: str, titulo: str, local_url: str) -> None:
        if fisico:
            novo_livro = LivroFisico(autor, assunto, resumo, isbn,
                                     titulo, local_url)
        else:
            novo_livro = LivroEletronico(autor, assunto, resumo, isbn,
                                         titulo, local_url)
        self.__livros.append(novo_livro)

    def remover_titulo(self, titulo: str) -> None:
        livro = self.buscar_titulo(titulo)
        if livro is not None:
            self.__livros.remove(livro)
            return None

    def emprestar(self, titulo: str) -> bool:
        livro = self.buscar_titulo(titulo)
        if livro is not None and livro.emprestado == False:
            livro.emprestado = True
            return True
        return False
    
    def devolver(self, titulo: str) -> bool:
        livro = self.buscar_titulo(titulo)
        if livro is not None and livro.emprestado:
            livro.emprestado = False
            return True
        return False

    def mudar_situacao(self, titulo: str) -> bool:
        livro = self.buscar_titulo(titulo)
        if livro is not None:
            livro.emprestado = not livro.emprestado
            return True
        return False

    def listar(self) -> None:
        print(id(self.__livros))
        return self.__livros[:]

