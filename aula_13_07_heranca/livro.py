class Livro(object):

    def __init__(self, autor: str, assunto: str,
                 resumo: str, isbn: str, titulo: str) -> None:
        self.autor = autor
        self.assunto = assunto
        self.resumo = resumo
        self.isbn = isbn
        self.titulo = titulo
        self.emprestado = False

    def imprimir_dados(self) -> None:
        raise NotImplementedError


class LivroEletronico(Livro):

    def __init__(self, autor: str, assunto: str, resumo: str,
                 isbn: str, titulo: str, url: str) -> None:
        super().__init__(autor, assunto, resumo, isbn, titulo)
        self.url = url

    def verificarUrl(self) -> None:
        print("Verificando url")

    def imprimir_dados(self) -> None:
        print(self.url)

class LivroFisico(Livro):

    def __init__(self, autor: str, assunto: str, resumo: str, isbn: str,    
                 titulo: str, localizacao: str) -> None:
        super().__init__(autor, assunto, resumo, isbn, titulo)
        self.localizacao = localizacao

    def mostrar_estante(self) -> str:
        return self.localizacao[0:3]

    def mostrar_pratileira(self) -> str:
        return self.localizacao[4:] 
