from figurinha import Figurinha


class GerenciadorFigurinha:

    def __init__(self) -> None:
        self.lista_figurinhas = []

    def adicionar_figurinha(self, nome_album: str, numero: int) -> bool:
        nova_fig = Figurinha(numero, nome_album)
        if not self.procurar_figurinha(nova_fig):
            self.lista_figurinhas.append(nova_fig)
            return True
        return False

    def procurar_figurinha(self,  figurinha: Figurinha) -> bool:
        for fig in self.lista_figurinhas:
            if fig == figurinha:
                return True
        return False

    def remover_figurinha(self, nome_album: str, numero: int) -> bool:
        for fig in self.lista_figurinhas:
            if fig.numero == numero and fig.nome_album == nome_album:
                self.lista_figurinhas.remove(fig)
                return True
        return False

    def imprimir_lista(self):
        for fig in self.lista_figurinhas:
            print("Figurinha do álbum ", fig.nome_album, " número ", fig.numero)
        print("------------fim da lista-----------------")


def funcao2():
    print("função 2")


if __name__ == "__main__":
    gen = GerenciadorFigurinha()
    print(gen.adicionar_figurinha("a", 1))
    print(gen.adicionar_figurinha("a", 2))
    print(gen.adicionar_figurinha("a", 3))
    print(gen.adicionar_figurinha("a", 4))
    print(gen.adicionar_figurinha("a", 5))
    print(gen.adicionar_figurinha("b", 1))
    print(gen.adicionar_figurinha("b", 1))
    print(gen.adicionar_figurinha("b", 2))
    print(gen.adicionar_figurinha("b", 3))
    print(gen.adicionar_figurinha("b", 4))
    gen.imprimir_lista()
    print(gen.remover_figurinha("a", 2))
    print(gen.remover_figurinha("c", 2))
    gen.imprimir_lista()
