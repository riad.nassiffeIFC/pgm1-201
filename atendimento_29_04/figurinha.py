class Figurinha:

    def __init__(self, numero: int, nome_album: str) -> None:
        self.numero = numero
        self.nome_album = nome_album

    def __eq__(self, o: object) -> bool:
        if self.numero == o.numero and self.nome_album == o.nome_album:
            return True
        return False
