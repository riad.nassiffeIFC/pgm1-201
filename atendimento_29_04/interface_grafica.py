from gerenciador_figurinhas import GerenciadorFigurinha


class InterfaceGerenciador:

    def __init__(self) -> None:
        self.gen = GerenciadorFigurinha()

    def imprimir_menu(self):
        print("-------------------Menu--------------")
        print("|        1- Cadastrar nova figurinha|")
        print("|        2- Remover nova figurinha  |")
        print("|        3- Listar nova figurinha   |")
        print("|        4- Sair                    |")
        print("-------------------------------------")

    def cadastrar_figurinha(self):
        nome_album = input("Nome do álbum ")
        numero = int(input("Número da figurinha: "))
        if self.gen.adicionar_figurinha(nome_album, numero):
            print("Figurinha cadastrada com sucesso :)")
        else:
            print("Erro ao cadastrar a figurinha :(")

    def remover_figurinha(self):
        nome_album = input("Nome do álbum ")
        numero = int(input("Número da figurinha: "))
        if self.gen.remover_figurinha(nome_album, numero):
            print("Figurinha removida com sucesso :)")
        else:
            print("Erro ao remover a figurinha :(")

    def listar_figurinha(self):
        for fig in self.gen.lista_figurinhas:
            print("Figurinha do álbum ", fig.nome_album, " número ", fig.numero)
        print("------------fim da lista-----------------")

    def main(self):
        sair = False
        while not sair:
            self.imprimir_menu()
            opcao = int(input("O que você deseja fazer: "))
            if opcao == 1:
                self.cadastrar_figurinha()
            elif opcao == 2:
                self.remover_figurinha()
            elif opcao == 3:
                self.listar_figurinha()
            elif opcao == 4:
                sair = True
            else:
                print("Opção inválida!")


if __name__ == "__main__":
    inter = InterfaceGerenciador()
    inter.main()
