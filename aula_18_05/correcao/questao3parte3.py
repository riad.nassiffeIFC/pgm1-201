from questao3parte2 import Biblioteca

class IntGrafica(object):

    def __init__(self) ->None:
        self.bib = Biblioteca()

    def adicionar_livro(self) -> None:
        fisico = input("Digite o tipo: ")
        if fisico == "fisico":
            fisico = True
        else:
            fisico = False 
        autor = input("Digite o nome do autor: ") 
        assunto = input("Digite o assunto: ") 
        resumo = input("Digite o resumo: ") 
        isbn = input("Digite o isbn: ") 
        titulo = input("Digite o titulo: ")
        if self.bib.adicionar(fisico, autor, assunto, resumo, isbn, titulo):
            print("Livro cadastrado com sucesso!!")
        else:
            print("Livro já existente!")

    def remover_titulo(self) -> None:
        titulo = input("Digite o título: ")
        if self.bib.remover_titulo(titulo):
            print("Livro removido com sucesso!")
        else:
            print("Livro não encontrado!")

#parte q eu fiz
    def emprestar_olivro(self) -> None:
        titulo = input("Digite o título: ")
        self.bib.emprestar_livro(titulo)

    def devolver_olivro(self) -> None:
        titulo = input("Digite o título: ")
        self.bib.devolver_livro(titulo)
#vai até aqui

    def listar_todos(self) -> None:
        livros = self.bib.listar()
        for livro in livros:
            print("--------------------")
            print("Título: ", livro.titulo)
        print("fim da lista de livros")

#parte q eu fiz
    def listar_emprestados(self) -> None:
        livros = self.bib.listar()
        for livro in livros:
            if livro.emprestado:
                print("--------------------")
                print("Título: ", livro.titulo)
        print("fim da lista de livros")

    def listar_devolvidos(self) -> None:
        livros = self.bib.listar()
        for livro in livros:
            if not livro.emprestado:
                print("--------------------")
                print("Título: ", livro.titulo)
        print("fim da lista de livros")
#vai até aqui

    def imprimir_menu(self) -> None:
        print()
        print("-------------------Menu------------------------")
        print("|        1- Cadastrar Novo Livro              |")
        print("|        2- Remover Livro                     |")
        print("|        3- Emprestar Livro                   |")
        print("|        4- Devolver Livro                    |")
        print("|        5- Listar: todos os livros           |")
        print("|        6- Listar: livros emprestados        |")
        print("|        7- Listar: livros devolvidos         |")
        print("|        8- Sair                              |")
        print("-----------------------------------------------")
        print()

    def main(self):
        sair = False
        while not sair:
            self.imprimir_menu()
            opcao = int(input("O que você deseja fazer: "))
            if opcao == 1:
                self.adicionar_livro()
            elif opcao == 2:
                self.remover_titulo()
            elif opcao == 3:
                self.emprestar_olivro()
            elif opcao == 4:
                self.devolver_olivro()
            elif opcao == 5:
                self.listar_todos()
            elif opcao == 6:
                self.listar_emprestados()
            elif opcao == 7:
                self.listar_devolvidos()
            elif opcao == 8:
                sair = True
            else:
                print("Opção inválida!")

if __name__ == "__main__":
    app = IntGrafica()
    app.main()