from questao2parte1 import Produto

class Gerenciador(object):
    def __init__(self) -> None:
        self.estoque = []

    def pesquisar_produto(self, nome:str) -> Produto:
        for produto in self.estoque:
            if produto.nome == nome:
                return produto
        return None

    def cadastrar_produto(self) -> bool:
        nome = input("Informe o nome do produto: ")
        codigo = input("Informe o codigo do produto: ")
        valor = input("Informe o valor do produto: ")
        quantidade = input("Informe a quantidade do produto: ")
        if self.pesquisar_produto(nome) is None:
            self.estoque.append(Produto(nome, codigo, valor, quantidade))
            print("Produto cadastrado.")
        else:
            print("Produjo ja cadastrado.")

    def adicionar_nvquantidade(self, nome: str, quantidade: int) -> bool:
        nome = input("Informe o produto que você deseja adicionar uma nova quantidade: ")
        produto = self.pesquisar_produto(nome)
        if produto is not None:
            produto.quantidade += quantidade
            return True
        return False

    def remover_nvquantidade(self, nome: str, quantidade: int) -> bool:
        nome = input("Informe o produto que você deseja remover uma quantidade: ")
        produto = self.pesquisar_produto(nome)
        if produto is not None:
            produto.quantidade -= quantidade
            return True
        return False

    def imprimir_estoque(self) -> None:
        for prod in self.estoque:
            print("_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_")
            print("Nome do produto:",prod.nome,"-- Código do produto:",prod.codigo,"-- Valor do produto:",prod.valor,
                  "-- Quantidade do produto:",prod.quantidade)
        print("_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_")

    def imprimir_preview(self) -> None:
        print()
        print("------------------------Menu-------------------------")
        print("|     1- Cadastrar Podruto                          |")
        print("|     2- Pesquisar Produto                          |")
        print("|     3- Adicionar Nova Quantidade a um Produto     |")
        print("|     4- Remover Nova Quantidade a um Produto       |")
        print("|     5- Listar Produtos em Estoque                 |")
        print("|     6- Sair                                       |")
        print("-----------------------------------------------------")
        print()

    def main(self):
        sair = False
        while not sair:
            self.imprimir_preview()
            pergunta = int(input("Digite um número do menu para efetuar uma ação: "))
            if pergunta == 1:
                self.cadastrar_produto()
            elif pergunta == 2:
                self.pesquisar_produto()
            elif pergunta == 3:
                self.adicionar_nvquantidade()
            elif pergunta == 4:
                self.remover_nvquantidade()
            elif pergunta == 5:
                self.imprimir_estoque()
            elif pergunta == 6:
                sair = True
            else:
                print("Número não cadastrado no Menu.")

if __name__ == "__main__":
    rodar = Gerenciador()
    rodar.main()
