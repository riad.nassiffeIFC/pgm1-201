from livro import Livro

def test_criar():
    livro = Livro(True, "João", "Matemática", "blablabla", "2435234",
                  "Blabla da Matemática")
    assert livro.titulo == "Blabla da Matemática"

def test_modificar():
    livro = Livro(True, "João", "Matemática", "blablabla", "2435234",
                  "Blabla da Matemática")
    assert livro.assunto == "Matemática"
    livro.assunto = "Física"
    assert livro.assunto == "Física"
