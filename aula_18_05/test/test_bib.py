from biblioteca import Biblioteca

def test_criar_bib():
    bib = Biblioteca()
    lista = bib.listar()
    assert len(lista) == 0

def test_adicionar():
    bib = Biblioteca()
    assert bib.adicionar(True, "Pedro", "Jogo", "sdfgsfgsdfgs", 
                         "342342354", "Teste") == None

def test_buscar():
    bib = Biblioteca()
    bib.adicionar(True, "Pedro", "Jogo", "sdfgsfgsdfgs", "342342354",
              "Blabla dos Jogos")
    bib.adicionar(True, "Maria", "Inglês", "qetqret", "555555",
              "Blabla do Inglês")
    bib.adicionar(True, "Alan", "Geografia", "wretwertwer", 
                      "666666","Blabla da Geografia")
    bib.adicionar(True, "José", "Física", "wretwret", "2345234",
              "Blabla da Física")
    bib.adicionar(True, "João", "Matemática", "blablabla",
                      "2435234", "Blabla da Matemática")
    livro = bib.buscar_titulo("Blabla da Física")
    assert livro.titulo == "Blabla da Física"
    assert bib.buscar_titulo("Blabla da Físic") is None