from biblioteca import Biblioteca

class IntGrafica(object):

    def __init__(self) ->None:
        self.bib = Biblioteca()

    def adicionar(self) -> None:
        fisico = input("Digite o tipo: ")
        if fisico == "fisico":
            fisico = True
        else:
            fisico = False 
        autor = input("Digite o nome do autor: ") 
        assunto = input("Digite o assunto: ") 
        resumo = input("Digite o resumo: ") 
        isbn = input("Digite o isbn: ") 
        titulo = input("Digite o titulo: ")
        self.bib.adicionar(fisico, autor, assunto, resumo, isbn, titulo)
        
    def remover_titulo(self) -> None:
        titulo = input("Digite o título: ")
        self.bib.remover_titulo(titulo)

    def emprestar_titulo(self) -> None:
        titulo = input("Digite o título: ")
        self.bib.emprestar(titulo)

    def devolver_titulo(self) -> None:
        titulo = input("Digite o título: ")
        self.bib.devolver(titulo)

    def listar(self) -> None:
        livros = self.bib.listar()
        for livro in livros:
            print("--------------------")
            print("Título: ", livro.titulo)
        print("fim da lista de livros")
    
    def listar_emprestados(self) -> None:
        livros = self.bib.listar()
        for livro in livros:
            if livro.emprestado:
                print("--------------------")
                print("Título: ", livro.titulo)
        print("fim da lista de livros")

    def listar_nao_emprestados(self) -> None:
        livros = self.bib.listar()
        for livro in livros:
            if not livro.emprestado:
                print("--------------------")
                print("Título: ", livro.titulo)
        print("fim da lista de livros")

    def imprimir_menu(self) -> None:
        print("-------------------Menu-------------------")
        print("|        1- Cadastrar novo livro          |")
        print("|        2- Remover livro                 |")
        print("|        3- Emprestar                     |")
        print("|        4- Devolver                      |")
        print("|        5- Listar livros                 |")
        print("|        6- Listar livros Emprestados     |")
        print("|        7- Listar livros não Emprestados |")
        print("|        8- Sair                          |")
        print("------------------------------------------")

    def main(self):
        sair = False
        while not sair:
            self.imprimir_menu()
            opcao = int(input("O que você deseja fazer: "))
            if opcao == 1:
                self.adicionar()
            elif opcao == 2:
                self.remover_titulo()
            elif opcao == 3:
                self.emprestar_titulo()
            elif opcao == 4:
                self.devolver_titulo()
            elif opcao == 5:
                self.listar()
            elif opcao == 6:
                self.listar_emprestados()
            elif opcao == 7:
                self.listar_nao_emprestados()
            elif opcao == 8:
                sair = True
            else:
                print("Opção inválida!")

if __name__ == "__main__":
    app = IntGrafica()
    app.main()