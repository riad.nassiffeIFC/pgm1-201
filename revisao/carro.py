class Carro(object):

    def __init__(self, modelo: str, marca: str, ano: int) -> None:
        self.modelo = modelo
        self.marca = marca
        self.__ano = ano

    def set_ano(self, ano: int):
        """Redefine o valor da variável ano, não deixando ele ser menor que 0.

        Args:
            ano (int): número inteiro que representa o ano.
        """
        if ano > 0:
            self.__ano = ano

    def __eq__(self, objeto):
        if self.marca == objeto.marca:
            return True
        return False

    def __repr__(self):
        return "Modelo {}, marca {} e ano {}".format(self.modelo, self.marca, self.__ano)

c = Carro("Gol", "VW", 1998)
c1 = Carro("Gol", "VW", 1998)
