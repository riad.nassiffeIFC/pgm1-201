from animal import Animal
from cachorro import Cachorro

class Peixe(Animal):

    def __init__(self, cor: str, nome: str, tipo_agua: str):
        Animal.__init__(self, cor, nome)
        self.tipo_agua = tipo_agua

    def fazer_barulho(self):
        print("glub glub")

class PeixeDourado(Peixe):

    def __init__(self, nome):
        super().__init__("Dourado", nome, "doce")

class PeixeCao(Peixe, Cachorro):

    def __init__(self, cor, nome, tipo_agua, raca):
        Peixe.__init__(self, cor, nome, tipo_agua)
        Cachorro.__init__(self, cor, nome, raca)

if __name__ == "__main__":
    """nemo = PeixeDourado("nemo")
    nemo2 = PeixeDourado("nemo2")
    nemo.fazer_barulho()
    print(nemo == nemo2)"""
    peixe_guarda = PeixeCao("cinza", "bob", "salgada", "pitbull")
    peixe_guarda.fazer_barulho()