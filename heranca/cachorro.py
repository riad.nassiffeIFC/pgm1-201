from animal import Animal

class Cachorro(Animal):

    def __init__(self, cor: str, nome: str, raca: str):
        super().__init__(cor, nome)
        self.raca = raca

    def fazer_barulho(self):
        super().fazer_barulho()
        print("Auau")

if __name__ == "__main__":
    scooby = Cachorro("marrom", "scooby", "vira lata")
    scooby.fazer_barulho()
    scooby.mostrar_cor()
    print(scooby.nome)
