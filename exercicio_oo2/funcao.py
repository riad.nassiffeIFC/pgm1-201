class Funcao(object):

    def __init__(self, a:float, b:float, c:float) -> None:
        """
            a*x^2 + b*x + c
        """
        self. a = a
        self.b = b
        self.c = c

    def calcular_raizes(self) -> tuple:
        delta = self.b**2 - 4* self.a * self.c
        """delta == 0 -> duas raízes iguais
           delta < 0  -> não existe raízes
           delta > 0  -> duas raízes reais
        """
        if delta == 0:
            raiz1 = (-self.b)/(2*self.a)
            return (raiz1,)
        elif delta > 0:
            raiz1 = (-self.b + delta**0.5)/(2*self.a)
            raiz2 = (-self.b - delta**0.5)/(2*self.a)
            return (raiz1, raiz2)
        else:
            return tuple()

if __name__ == "__main__":
    f1 = Funcao(24, 12, -30)
    print(f1.calcular_raizes())