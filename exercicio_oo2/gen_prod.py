from produto import Produto

class GerenciadorProd(object):

    def __init__(self) -> None:
        self.produtos = []

    def buscar_nome(self, nome: str) -> Produto:
        for produto in self.produtos:
            if produto.nome == nome:
                return produto
        return None

    def cadastrar_produto(self, nome: str, codigo: int, 
                          valor: float, quantidade: int) -> bool:
        if self.buscar_nome(nome) is None:
            novo_produto = Produto(nome, codigo, valor, quantidade)
            self.produtos.append(novo_produto)
            return True
        return False

    def adicionar_item(self, nome: str, quantidade: int) -> bool:
        produto = self.buscar_nome(nome)
        if produto is not None:
            produto.quantidade += quantidade
            return True
        return False

    def remover_item(self, nome: str, quantidade: int) -> bool:
        produto = self.buscar_nome(nome)
        if produto is not None:
            produto.quantidade -= quantidade
            return True
        return False 