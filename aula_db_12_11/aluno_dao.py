import sqlite3
from sqlite3.dbapi2 import Connection
from aluno import Aluno
from config import sql_path



class AlunoDao:

    def __init__(self) -> None:
        self.con = None

    def conectar(self):
        if self.con is None:
            #arq = open("config.txt", "r")
            #banco = arq.readline().replace('\n', "")
            self.con = sqlite3.connect(sql_path)
            #arq.close()
        

    def desconectar(self):
        if self.con is not None:
            self.con.close()
            self.con = None

    def criar_tabela(self):
        self.conectar()
        self.con.execute("""
                    CREATE TABLE alunos (
                    nome TEXT NOT NULL,
                    idade INTEGER,
                    cpf     VARCHAR(11) PRIMARY KEY NOT NULL,
                    email TEXT NOT NULL
                    );
                    """)
        self.desconectar()

    def inserir(self, aluno:Aluno):
        self.conectar()
        dados = (aluno.nome, aluno.idade,
                 aluno.cpf, aluno.email)

        self.con.execute("insert into alunos "\
                         "(nome, idade, cpf, email)"\
                         "values (?, ?, ?, ?)", dados)
        self.con.commit()
        self.desconectar()

    def buscar(self, cpf):
        variavel = []
        for reg in kwargs.items():
            variavel.append(reg)
        self.conectar()
        sql = "select * from alunos "\
              "where cpf = ?"
        result = self.con.execute(sql,(cpf,)
        resultado = []
        for reg in result:
            resultado.append(Aluno(reg[1],reg[2],reg[3],
                             reg[4]))
        self.desconectar()
        return resultado


    def deletar_cpf(self, cpf:str):
        self.conectar()
        self.con.execute("delete from alunos where cpf = ?", (cpf,))
        self.con.commit()
        self.desconectar()


    def atualizar(self, aluno:Aluno):
        self.conectar()
        self.con.execute('update alunos set idade = ?, nome = ?, email = ? where cpf = ?',
                         (aluno.idade, aluno.nome, aluno.email, aluno.cpf))
        self.con.commit()
        self.desconectar()

if __name__ == "__main__":
    aluno_dao = AlunoDao()
    #aludo_d.criar_tabela()
    a = Aluno("Julio", 17, "021.666.534-11", "p@p.com")
    #aluno_dao.inserir(a)
    #aluno_dao.deletar_cpf("234.423.534-34")
    resultado = aluno_dao.buscar(cpf="021.666.534-33")
    if len(resultado) > 0:
        aluno = resultado[0]
        aluno.idade = 21
        aluno.email = "j@j.com.br"
        aluno_dao.atualizar(aluno)
    
    for aluno in resultado:
        print("nome {} e idade {} ".format(aluno.nome, aluno.idade))
    
