class Torneira(object):

    def __init__(self, nome: str, entrada: bool,
                 vazao: float) -> None:
        self.nome = nome
        self.__entrada = entrada
        self.__vazao = vazao
        self.__aberta = False 

    def retornar_entrada(self) -> str:
        return self.__entrada

    def definir_entrada(self, nova_entrada: bool) -> None:
        if nova_entrada == True or nova_entrada == False:
            self.__entrada = nova_entrada

    def abrir(self) -> None:
        self.aberta = True

    def fechar(self) -> None:
        self.aberta = False

    def retornar_status(self) ->bool:
        return self.__aberta