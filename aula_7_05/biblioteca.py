from livro import Livro

class Biblioteca(object):

    def __init__(self):
        self.__livros = []

    def buscar_titulo(self, titulo: str) -> Livro:
        for livro in self.__livros:
            if livro.titulo == titulo:
                return livro
        return None

    def adicionar(self, fisico: bool, autor: str, assunto: str,
                  resumo: str, isbn: str, titulo: str) -> bool:
        if self.buscar_titulo(titulo) is None:
            novo_livro = Livro(fisico, autor, assunto, resumo, isbn, titulo)
            self.__livros.append(novo_livro)
            return True
        return False

    def remover_titulo(self, titulo: str) -> bool:
        livro = self.buscar_titulo(titulo)
        if livro is not None:
            self.__livros.remove(livro)
            return True
        return False

    def listar(self) -> list:
        print(id(self.__livros))
        return self.__livros[:]