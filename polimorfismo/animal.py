class Animal(object):

    def __init__(self, cor: str, nome: str):
        self.cor = cor
        self.nome = nome

    def fazer_barulho(self):
        print("tectec")

    def mostrar_cor(self):
        print(self.cor)

    def __eq__(self, animal):
        if self.nome == animal.nome:
            return True
        return False