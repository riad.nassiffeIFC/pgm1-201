from animal import Animal
from cachorro import Cachorro

class Peixe(Animal):

    def __init__(self, cor: str, nome: str, tipo_agua: str):
        Animal.__init__(self, cor, nome)
        self.tipo_agua = tipo_agua

    def fazer_barulho(self):
        print("glub glub")

    def nadar(self):
        raise NotImplementedError

class PeixeDourado(Peixe):

    def __init__(self, nome):
        super().__init__("Dourado", nome, "doce")

    def nadar(self):
        print("Peixe dourado nadando devagar.")

class PeixeCao(Peixe, Cachorro):

    def __init__(self, cor, nome, tipo_agua, raca):
        Peixe.__init__(self, cor, nome, tipo_agua)
        Cachorro.__init__(self, cor, nome, raca)

    #def nadar(self):
    #    print("Peixe cão nada rápido!")
"""
if __name__ == "__main__":
    nemo = PeixeDourado("nemo")
    nemo2 = PeixeDourado("nemo2")
    peixe_guarda = PeixeCao("cinza", "bob", "salgada", "pitbull")
    scooby = Cachorro("marrom", "scooby", "vira lata")
    lista_animal = [nemo, nemo2, peixe_guarda, scooby]
    
    #Na programação orientada a objetos, o polimorfismo permite que 
    #referências de tipos de classes mais abstratas representem o 
    #comportamento das classes concretas que referenciam (Animal).
    
    for animal in lista_animal:
        print(animal.nome)
        animal.fazer_barulho()
"""    

"""Tratamento de exceções"""
if __name__ == "__main__":
    nemo = PeixeDourado("nemo")
    nemo2 = PeixeDourado("nemo2")
    peixe_guarda = PeixeCao("cinza", "bob", "salgada", "pitbull")
    lista_animal = [peixe_guarda, nemo, nemo2]
    
    try:
        #a = 3/0
        for animal in lista_animal:
            print(animal.nome)
            animal.nadar()
    except NotImplementedError:
        print("Alguém esqueceu de implementar o método nadar!")
    except :
        print("Deu algum erro não trado, mas vou continuar funcioando!")
    finally:
        print("Executo indepentende de erro.")