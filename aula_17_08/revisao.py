class Usuario(object):

    def __init__(self, nome:str, senha:str) -> None:
        """Inicializa um objeto da classe usuário

        Args:
            nome (str): nome de usuário, deve ter no máximo 30 letras
            senha (str): representa a senha do usuário e deve ter letras e número
        """
        self.nome = nome
        self.__senha = senha

    def mudar_senha(self, senha:str):
        self.__senha = senha

    def validar_senha(self, senha):
        if senha == self.__senha:
            return True
        False

    def recuperar_senha(self):
        return self.__senha

    def __repr__(self) -> str:
        return "Nome:"+ self.nome

class Aluno(Usuario):

    def __init__(self, nome: str, senha: str, turma:str) -> None:
        super().__init__(nome, senha)
        self.turma = turma

    def mudar_senha(self, senha: str):
        if (len(senha) >= 6):
            self.__senha = senha

class Professor(Usuario):

    def __init__(self, nome: str, senha: str, disciplina:str) -> None:
        super().__init__(nome, senha)
        self.disciplina = disciplina

    def validar_senha(self, senha:str):
        if (senha.upper() == self.__senha):
            return True

    def recuperar_senha(self):
        return self.__senha.upper()

try:
    arq = open("teste", "r")
    lista = ["asdf asd\n", "asd2sdfa\n", "1234af\n"]
    texto = arq.read()
    print(texto)
except FileNotFoundError:
    print("Arquivo não encontrado!")
finally:
    try:
        arq.close()
    except NameError:
        pass

"""
while True:
    try:
        idade = int(input("Qual a sua idade? "))
        break
    except ValueError:
        print("Você digitou uma idade inválida")
    finally:
        print("Você esta quase terminando de preencher a ficha do cadastro!")


lista = []
u1 = Aluno("Paulo", "asfad", "201")
lista.append(u1)
u1 = Professor("Ríad", "ads1f", "Prog1")
lista.append(u1)
u1 = Aluno("Ricardo", "123", "201")
lista.append(u1)
u1 = Professor("Eder", "adsf", "Prog1")
lista.append(u1)

for usuario in lista:
    print(usuario)
"""

def imprimir(nome, *args):
    print(nome)
    print(args)

#imprimir("pedro", 1,2,3,45,56,6,6,"sfdg")

def imprimir2(nome, **kwargs):
    print(nome)
    print(kwargs)

imprimir2("pedro", n=1,m=2,idade=4, palavra="asfd")