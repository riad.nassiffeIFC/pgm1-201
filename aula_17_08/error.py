class ErroCustomizado(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(args)

class Usuario:

    def __init__(self, nome, senha) -> None:
        if str(nome[0]) == str(nome[0].lower()):
            raise ErroCustomizado("Todo nome deve começar com letra maiuscula")
        self.nome = nome
        self.senha = senha


try:
    nome = "paulo"
    senha = "fsdgar"
    usuario1 = Usuario(nome, senha)
except ErroCustomizado:
    nome = str(nome[0].upper()) + str(nome[1:])
    print(nome)
    usuario1 = Usuario(nome, senha)