from biblioteca import Biblioteca

class IntGrafica(object):

    def __init__(self) ->None:
        self.bib = Biblioteca()

    def adicionar(self) -> None:
        fisico = input("Digite o tipo: ")
        if fisico == "fisico":
            fisico = True
        else:
            fisico = False 
        autor = input("Digite o nome do autor: ") 
        assunto = input("Digite o assunto: ") 
        resumo = input("Digite o resumo: ") 
        isbn = input("Digite o isbn: ") 
        titulo = input("Digite o titulo: ")
        bib.adicionar(fisico, autor, assunto, resumo, isbn, titulo)
        
    def remover_titulo(self) -> None:
        titulo = input("Digite o título: ")
        bib.remover_titulo(titulo)

    def listar(self) -> None:
        livros = bib.listar()
        for livro in livros:
            print("--------------------")
            print("Título: ", livro.titulo)
        print("fim da lista de livros")

    def imprimir_menu(self) -> None:
        print("-------------------Menu--------------")
        print("|        1- Cadastrar novo livro    |")
        print("|        2- Remover livro           |")
        print("|        3- Listar livros           |")
        print("|        4- Sair                    |")
        print("-------------------------------------")

    def main(self):
        sair = False
        while not sair:
            self.imprimir_menu()
            opcao = int(input("O que você deseja fazer: "))
            if opcao == 1:
                self.adicionar()
            elif opcao == 2:
                self.remover()
            elif opcao == 3:
                self.listar()
            elif opcao == 4:
                sair = True
            else:
                print("Opção inválida!")

if __name__ == "__main__":
    app = IntGrafica()
    app.main()