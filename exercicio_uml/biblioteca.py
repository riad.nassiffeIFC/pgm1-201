from livro import Livro

class Biblioteca(object):

    def __init__(self):
        self.__livros = []

    def buscar_titulo(self, titulo: str) -> Livro:
        for livro in self.__livros:
            if livro.titulo == titulo:
                return livro
        return None

    def adicionar(self, fisico: bool, autor: str, assunto: str,
                  resumo: str, isbn: str, titulo: str) -> None:
        novo_livro = Livro(fisico, autor, assunto, resumo, isbn, titulo)
        self.__livros.append(novo_livro)

    def remover_titulo(self, titulo: str) -> None:
        livro = self.buscar_titulo(titulo)
        if livro is not None:
            self.__livros.remove(livro)
            return None

    def listar(self) -> None:
        print(id(self.__livros))
        return self.__livros[:]