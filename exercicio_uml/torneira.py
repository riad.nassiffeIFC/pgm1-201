class Torneira(object):

    def __init__(self, nome: str, entrada: bool,
                 vazao: float) -> None:
        self.nome = nome
        self.entrada = entrada
        self.vazao = vazao
        self.aberta = False 

    def abrir(self) -> None:
        self.aberta = True

    def fechar(self) -> None:
        self.aberta = False