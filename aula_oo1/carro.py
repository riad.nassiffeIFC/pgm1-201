class Carro:

    def __init__(self, nome: str, ano: int) -> None:
        self.nome = nome
        self.ano = ano
        self.direcao = None

    def mudar_direcao(self, nova_direcao: str) -> None:
        if nova_direcao in ["norte", "sul", "leste", "oeste"]:
            self.direcao = nova_direcao
        print(self.direcao)

    def pegar_direcao(self) -> str:
        return self.direcao


fusca = Carro("Fusca", 19874)
uno = Carro("Uno",23423)
"""
print(fusca.nome)
print(uno.nome)
"""
fusca.mudar_direcao("sudeste")
fusca.mudar_direcao("sul")
direcao_fusca = fusca.pegar_direcao()
print(direcao_fusca)