class Data:

    def __init__(self, dia: int, mes: int, ano: int) -> None:
        self.dia = dia
        self.mes = mes
        self.ano = ano

    def set_dia(self, novo_dia: int) -> None:
        self.dia = novo_dia

    def get_dia(self) -> int:
        return self.dia

    def set_mes(self, novo_mes: int) -> None:
        self.mes = novo_mes

    def get_mes(self) -> int:
        return self.mes

    def set_ano(self, novo_ano: int) -> None:
        self.ano = novo_ano

    def get_ano(self) -> int:
        return self.ano

    def __repr__(self) -> str:
        return str(self.dia)+"/"+str(self.mes)+"/"+str(self.ano)

    def eh_bissexto(self) -> bool:
        if self.ano % 400 == 0:
            return True
        if self.ano % 4 == 0 and self.ano % 100 != 0:
            return True
        return False
        

    def avancar_dia(self) -> None:
        self.dia += 1
        if self.mes in [1, 3, 5, 7, 8, 10, 12]:
            if self.dia == 32:
                self.dia = 1
                self.mes += 1
                if self.mes == 13:
                    self.mes = 1
                    self.ano += 1
        elif self.mes in [4, 6, 9, 11]:
            if self.dia == 31:
                self.dia = 1
                self.mes += 1
        else:
            if self.eh_bissexto():
                if self.dia == 30:
                    self.dia = 1
                    self.mes += 1
            else:
                if self.dia == 29:
                    self.dia = 1
                    self.mes += 1
                    

