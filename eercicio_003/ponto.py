class Ponto:

    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def set_x(self, novo_x: float) -> None:
        self.x = novo_x

    def get_x(self) -> float:
        return self.x

    def set_y(self, novo_y: float) -> None:
        self.y = novo_y

    def get_y(self) -> float:
        return self.y

    def atualizar(self, x: float, y: float)-> None:
        self.x = x
        self.y = y

    def __eq__(self, outro) -> bool:
        if self.x == outro.x and self.y == outro.y:
            return True
        return False

    def __repr__(self) -> str:
        return "("+str(self.x) + ", " + str(self.y)+")"

    def calcular_distancia(self, ponto) -> float:
        return ((self.x - ponto.x)**2 + (self.y - ponto.y)**2)**.5