class Receita:

    def __init__(self, nome:str, lista_ingedientes: list, modo_preparo: str) -> None:
        self.nome = nome
        self.ingredientes = lista_ingedientes
        self.preparo = modo_preparo

class Caderno:

    def __init__(self):
        self.receitas = []

    def adicionar_receita(self, nome: str, lista_ingedientes: list, modo_preparo: str) -> None:
        receita = Receita(nome, lista_ingedientes, modo_preparo)
        self.receitas.append(receita)