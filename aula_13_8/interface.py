from mercado import Mercado

class InterfaceMercado(object):

    def __init__(self) -> None:
        nome_mercado = input("Digite o nome do mercado:")
        self.__mercado = Mercado(nome_mercado)

    def pegar_float(self, msg:str)->float:
        while True:
            try:
                numero = float(input(msg))
                return numero
            except ValueError:
                print("Valor inválido, por favor digite um número válido.")

    def pegar_inteiro(self, msg:str)->int:
        while True:
            try:
                numero = int(input(msg))
                return numero
            except ValueError:
                print("Valor inválido, por favor digite um número inteiro válido.")

    def cadastrar_produto(self):
        tipo_produto = input("Qual tipo de produto você deseja cadastrar(1-perecível, 2 não perecível)?")
        nome = input("Qual o nome? ")
        codigo = input("Qual o código? ")
        descricao = input("Qual a descrição? ")
        valor = self.pegar_float("Qual o valor? ")
        if tipo_produto == "1":
            validade_data = input("Qual a validade? ")
            resultado = self.__mercado.cadastrar_produto(nome, codigo, descricao, valor,        
                                    validade=validade_data)
        else:
            tipo_unidade = input("Se peso p, caso unidade u:")
            if tipo_produto == "p":
                tipo_unidade = False
            else:
                tipo_unidade = True
            quantidade_produto = self.pegar_float("Qual a quantidade? ")
            resultado = self.__mercado.cadastrar_produto(nome, codigo, descricao, valor, 
                                             quantidade=quantidade_produto,
                                             unidade=tipo_unidade)
        if resultado:
            print("Produto cadastrado com sucesso!!!")
        else:
            print("Produto não cadastrado, porque o código já esta em uso.")

    def remover(self):
        codigo = input("Qual o código do produto que você deseja remover? ")
        if self.__mercado.remover_produto(codigo):
            print("Produto removido com sucesso!")
        else:
            print("Código não encontrado.")

    def salvar(self):
        nome_arq = input("Qual nome do arquivo: ")
        resultado = self.__mercado.gravar(nome_arq)
        if (resultado == -3):
            print("O usuário não tem permissão para gravar.")
        elif(resultado == -2):
            print("Ocorreu algum erro inesperado.")
        elif(resultado == -1):
            print("O arquivo já existe, tente um outro nome.")
        else:
            print("Gravação ocorreu com sucesso!")

    def recuperar(self):
        nome_arq = input("Qual nome do arquivo: ")
        resultado = self.__mercado.recuperar(nome_arq)
        if resultado == -4:
            print("Arquivo inexistente.")
        elif(resultado == -3):
            print("O usuário não tem permissão para gravar.")
        elif(resultado == -2):
            print("Ocorreu algum erro inesperado.")
        else:
            print("A recuperação ocorreu com sucesso!")

    def buscar(self):
        codigo = input("Qual o código do produto? ")
        produto = self.__mercado.buscar(codigo)
        if produto is not None:
            print(produto)
        else:
            print("Produto não encontrado!")

    def listar_todos(self):
        self.__mercado.listar_produtos()

    def imprimir_menu(self):
        print("1 - Cadastrar")
        print("2 - Remover")
        print("3 - Buscar")
        print("4 - Salvar")
        print("5 - Recuperar")
        print("6 - Listar todos produtos")
        print("7 - Sair")

    def main(self):
        while True:
            self.imprimir_menu()
            op = self.pegar_inteiro("O que você deseja fazer? ")
            if op == 1:
                self.cadastrar_produto()
            elif op == 2:
                self.remover()
            elif op == 3:
                self.buscar()
            elif op == 4:
                self.salvar()
            elif op == 5:
                self.recuperar()
            elif op == 6:
                self.listar_todos()
            elif op == 7:
                print("Encerrando o aplicativo")
                break
            else:
                print("Comando inválido.")

if __name__ == "__main__":
    app = InterfaceMercado()
    app.main()