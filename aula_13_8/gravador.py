from produto import Produto, Perecivel, ProdutoNaoPerecivel


class Gravador:

    def __init__(self, nome_arq:str) -> None:
        self.nome_arq = nome_arq

    def gravar(self, lista_produtos:list):
        try:
            arquivo = open(self.nome_arq, "x")
            arquivo.close()
        except FileExistsError:
            print("Arquivo já existe, tente outro nome")
            return -1
        except OSError:
            print("Algo deu errado." + OSError.errno)
            return -2
        arquivo = open(self.nome_arq, "w")
        for produto in lista_produtos:
            arquivo.write(produto.converter_txt() + '\n')
        arquivo.close()
        return 0

    def recuperar(self, lista:list ):
        try:
            arquivo = open(self.nome_arq, "r")
            for linha in arquivo.readlines():
                linha = linha.replace("\n", "")
                info = linha.split("**")
                if len(info) == 6:
                    produto = ProdutoNaoPerecivel(info[0], info[1], info[2], float(info[3]), float(info[4]), bool(info[5]))
                else:
                    produto = Perecivel(info[0], info[1], info[2], float(info[3]), info[4])
                lista.append(produto)
            arquivo.close()
        except FileNotFoundError:
            print("Arquivo não encontrato!")
            return -3
        except OSError as err:
            print("Ocorreu algo de estranho, " + err.errno)
            return -2
        return 0