class Produto:


    def __init__(self, nome:str, codigo:str, descricao:str, valor: float) -> None:
        self.nome = nome
        self.codigo = codigo
        self.descricao = descricao
        self.valor = valor

    def converter_txt(self) ->str:
        return self.nome + "**" + self.codigo + "**" + self.descricao + "**" + str(self.valor)

    def __repr__(self) -> str:
        return "Nome: "+self.nome+"\n"+"Código: "+self.codigo+"\n"+ \
               "Descrição: "+self.descricao+"\n"+"Valor: "+str(self.valor)+"\n"

class Perecivel(Produto):

    def __init__(self, nome:str, codigo:str, descricao:str, valor: float, validade:str) -> None:
        super().__init__(nome, codigo, descricao, valor)
        self.validade = validade

    def converter_txt(self) ->str:
        produto_txt = super().converter_txt()
        return produto_txt + "**" + self.validade

    def __repr__(self) -> str:
        return super().__repr__()+"Validade: "+self.validade+"\n"+("*"*20)


class ProdutoNaoPerecivel(Produto):

    def __init__(self, nome:str, codigo:str, descricao:str, valor: float, quantidade: float, unidade: bool) -> None:
        super().__init__(nome, codigo, descricao, valor)
        self.quantidade = quantidade
        self.unidade = unidade

    def converter_txt(self) ->str:
        produto_txt = super().converter_txt()
        return produto_txt + "**" + str(self.quantidade) + "**" \
               + str(self.unidade)

    def __repr__(self) -> str:
        return super().__repr__()+"Unidade: "+str(self.unidade)+"\n" \
               +"Quanidade: "+str(self.quantidade)+"\n"+("*"*20)

    

if __name__ == "__main__":
    p1 = ProdutoNaoPerecivel("lápis", "34234", "da dasdf", 1.6,10,True)
    p2 = Perecivel("Queijo", "34234", "da dasdf", 36.9, "30/10/2021")
    print(p1)
    print(p2)