from endereco import Endereco

class Pessoa(object):

    def __init__(self, nome: str, idade: int, end: Endereco) -> None:
        self.nome = nome
        self.idade = idade
        self.end = end


casa = Endereco(54654, "sem nome", 0, "sc", "Brasil")
joao = Pessoa("João", 18, casa)
print(joao.end)
