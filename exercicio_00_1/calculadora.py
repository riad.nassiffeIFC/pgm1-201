class Calculadora(object):

    def __init__(self):
        self.ultimo_resultado = 0

    def somar(self, a: int, b: int) -> int:
        self.ultimo_resultado = a + b
        return self.ultimo_resultado

    def somar_n(self, *arg: list) -> int:
        self.ultimo_resultado = 0
        for numero in arg:
            self.ultimo_resultado += numero
        return self.ultimo_resultado


calculadora = Calculadora()
print(calculadora.somar(1, 2))
print(calculadora.somar_n(4, 4, 4, 4))
print(calculadora.somar_n(1, 2, 6, 7))
