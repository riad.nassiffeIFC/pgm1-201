class Data:

    def __init__(self, dd: int, mm: int, aa: int) -> None:
        self.dd = dd
        self.mm = mm
        self.aa = aa

    def definir_dia(self, dia):
        self.dia = dia

    def definir_mes(self, mes):
        self.mes = mes

    def definir_ano(self, ano):
        self.ano = ano

    def imprimir_brasil(self):
        return("Data formato brasileiro:" + str(self.dd) + "/" +
               str(self.mm) + "/" + str(self.aa))

    def imprimir_americano(self):
        return ("Data formato americano:" + str(self.mm) +
                "/" + str(self.dd) + "/" + str(self.aa))


hoje = Data(20, 4, 2021)
hoje.imprimir_americano()
hoje.imprimir_brasil()
