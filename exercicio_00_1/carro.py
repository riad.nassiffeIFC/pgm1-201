class Carro(object):

    def __init__(self, nome: str, marca: str, ano: str,
                 cor: str, placa: str) -> None:
        self.nome = nome
        self.marca = marca
        self.ano = ano
        self.velocidade = 0
        self.__cor = cor
        self.placa = placa

    def mudar_cor(self, nova_cor: str) -> None:
        if nova_cor in ["prata", "branco", "vermelho", "preto"]:
            self.__cor = nova_cor
        print(self.__cor)

    def mostrar_cor(self) -> str:
        return self.__cor

    def acelerar(self) -> None:
        self.velocidade += 1

    def freiar(self) -> None:
        if self.velocidade > 0:
            self.velocidade -= 1
        if self.velocidade < 0:
            self.velocidade += 1
