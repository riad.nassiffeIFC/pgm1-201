class Endereco(object):

    def __init__(self, cep: int, rua: str, numero: int, estado: str, pais: str,
                 complemento: str = None) -> None:
        self.cep = cep
        self.rua = rua
        self.numero = numero
        self.estado = estado
        self.pais = pais
        self.complemento = complemento


    def __eq__(self, end) -> bool:
        if self.rua == end.rua:
            return True
        return False

    # end1(self) == end2(end)  
    # end1.__eq__(end)  

    def __repr__(self) -> str:
        return "Rua:" + self.rua
