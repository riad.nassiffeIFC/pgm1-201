from gerenciador import Gerenciador


from gerenciador import Gerenciador
from jogo import Jogo


class Interface:

    def __init__(self) -> None:
        self.ge = Gerenciador()

    def menu(self):
        pass

    def adicionar_jogo(self):
        nome = input("Qual nome do jogo? ")
        jogo = Jogo(nome)
        self.ge.adicionar_jogo(jogo)

    def imprimir_jogos(self):
        for jogo in self.ge.lista_jogos:
            print(jogo.nome)

    def main(self):
        pass


if __name__ == "__main__":
    inter = Interface()
    inter.adicionar_jogo()
    inter.adicionar_jogo()
    inter.imprimir_jogos()
