from jogo import Jogo


class Gerenciador(object):

    def __init__(self) -> None:
        self.lista_jogos = []
        self.lista_desejo = []

    def adicionar_jogo(self, novo_jogo: Jogo) -> None:
        jogo = self.buscar_jogo(novo_jogo.nome)
        if jogo is None:
            self.lista_jogos.append(novo_jogo)

    def buscar_jogo(self, nome) -> Jogo:
        for jogo in self.lista_jogos:
            if jogo.nome == nome:
                return jogo
        return None

    def remover_jogo(self, nome) -> None:
        jogo = self.buscar_jogo(nome)
        if jogo is not None:
            self.lista_jogos.remove(jogo)
