class Produto:


    def __init__(self, nome:str, codigo:str, descricao:str, valor: float) -> None:
        self.nome = nome
        self.codigo = codigo
        self.descricao = descricao
        self.valor = valor

    def converter_txt(self) ->str:
        return self.nome + "**" + self.codigo + "**" + self.descricao + "**" + str(self.valor)

class Perecivel(Produto):

    def __init__(self, nome:str, codigo:str, descricao:str, valor: float, validade:str) -> None:
        super().__init__(nome, codigo, descricao, valor)
        self.validade = validade

    def converter_txt(self) ->str:
        produto_txt = super().converter_txt()
        return produto_txt + "**" + self.validade


class ProdutoNaoPerecivel(Produto):

    def __init__(self, nome:str, codigo:str, descricao:str, valor: float, quantidade: float, unidade: bool) -> None:
        super().__init__(nome, codigo, descricao, valor)
        self.quantidade = quantidade
        self.unidade = unidade

    def converter_txt(self) ->str:
        produto_txt = super().converter_txt()
        return produto_txt + "**" + str(self.quantidade) + "**" \
               + str(self.unidade)


if __name__ == "__main__":
    p1 = Produto("lápis", "34234", "da dasdf", 1.6)
    p2 = Perecivel("Queijo", "34234", "da dasdf", 36.9, "30/10/2021")
    print(p1.converter_txt())
    print(p2.converter_txt())