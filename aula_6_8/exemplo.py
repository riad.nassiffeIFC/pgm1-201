def func1(*args):
    print(args)

def func2(nome_escola:str, **kwargs):
    print(nome_escola)
    print(kwargs)

#func1(1,2,3,4,"2341", True)
#func1(12341234,3.4,"2341", True,"3234")
func2("IFC", nome="joão", idade=20, peso=78.9)
func2("IFC", nome="joão", idade=20, peso=78.9, sala= "201")
func2("IFC", nome="joão", idade=20, peso=78.9, ano=2021, cidade='Blumenau')