from produto import ProdutoNaoPerecivel, Perecivel, Produto
from gravador import Gravador

class Mercado(object):

    def __init__(self, nome: str)->None:
        self.__estoque = []
        self.nome = nome

    def buscar(self, codigo:str)->Produto:
        """Esse método busca um produto pelo código no estoque.

        Args:
            codigo (str): código do produto procurado.

        Returns:
            Produto: retornar um objeto produto ou None se não o encontrar.
        """
        for produto in self.__estoque:
            if produto.codigo == codigo:
                return produto
        return None
    """
        def func1(*lista_parametros):
            .....

        func1(1,2,3,4,"2341", True)
    """
    def cadastrar_produto(self, nome:str, codigo:str, descricao:str, 
                          valor: float, **kwargs)->bool:
        """Cadastra um produto na venda.

        Args:
            nome (str): nome do produto
            codigo (str): código do produto
            descricao (str): descrição do produto
            valor (float): valor do produto

        Returns:
            bool: returna True se conseguir adicionar e False se não conseguir.
        """
        if self.buscar(codigo) is not None:
            return False
        else:
            if kwargs.get("validade"):
                produto = Perecivel(nome, codigo, descricao, valor, kwargs.get("validade"))
            else:
                produto = ProdutoNaoPerecivel(nome, codigo, descricao, valor, kwargs.get("quantidade"), kwargs.get("unidade"))
            self.__estoque.append(produto)
        return True

    def remover_produto(self, codigo:str)-> bool:
        """Esse método remove um produto do estoque.

        Args:
            codigo (str): código do produto a ser removido.

        Returns:
            bool: True se o produto foi removido e False se o produto não existe no estoque.
        """
        produto = self.buscar(codigo)
        if produto is not None:
            self.__estoque.remove(produto)
            return True
        return False

    def gravar(self, nome_arq:str) ->int:
        """Grava o estoque em um arquivo txt, para futura recuperação

        Args:
            nome_arq (str): nome do arquivo onde os dados vão ser gravados, ele não pode existir.

        Returns:
            int: É retornado:
                    + -3 - se o usuário não tiver permissão.
                    + -2 - se algum erro inesperado ocorrer.
                    + -1 - se o arquivo já existir.
                    + 0  - se tudo ocorrer normalmente.
        """
        gravador = Gravador(nome_arq)
        return gravador.gravar(self.__estoque)

    def recuperar(self, nome_arq:str) ->int:
        """Recupera o estoque que foi gravado em um arquivo txt.

        Args:
            nome_arq (str): nome do arquivo para recuperar as informações.

        Returns:
            int: É retornado:
                    + -4 - se o arquivo não existir.
                    + -3 - se o usuário não tiver permissão.
                    + -2 - se algum erro inesperado ocorrer.
                    + 0  - se tudo ocorrer normalmente.
        """
        gravador = Gravador(nome_arq)
        self.__estoque = []
        return gravador.recuperar(self.__estoque)