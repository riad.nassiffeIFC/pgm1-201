class Livro(object):

    def __init__(self, fisico: bool, autor: str, assunto: str,
                 resumo: str, isbn: str, titulo: str) -> None:
        self.fisico = fisico
        self.autor = autor
        self.assunto = assunto
        self.resumo = resumo
        self.isbn = isbn
        self.titulo = titulo
        self.emprestado = False

    def converter_str(self):
        return str(self.fisico) +"  "+ self.autor +"  "+ self.assunto +"  "+self.resumo +"  "+ self.isbn +"  "+ self.titulo +"  "+ str(self.emprestado)