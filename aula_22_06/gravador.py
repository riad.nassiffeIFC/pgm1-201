from livro import Livro

class GravadorDeLivros(object):

    def gravar(self, lista_livros) -> None:
        arquivo = open("lista_livros", "w")
        for livro in lista_livros:
            arquivo.write(livro.converter_str()+"\n")
        arquivo.close()

    def recuperar(self) -> list:
        arquivo = open("lista_livros", "r")
        lista = []
        for linha in arquivo.readlines():
            linha = linha.replace("\n", "")
            info = linha.split("  ")
            livro = Livro(info[0], info[1], info[2], info[3], info[4], info[5])
            livro.emprestado = info[6]
            lista.append(livro)
        arquivo.close()
        return lista
